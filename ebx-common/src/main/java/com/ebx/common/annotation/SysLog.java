
package com.ebx.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 系统操作日志记录
 * 
 * @author hubin
 * @date 2018-04-23
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {
    
    /** 模块 */
    int type() default 0;
    
    /** 操作说明 */
    String title() default "";
    
}
