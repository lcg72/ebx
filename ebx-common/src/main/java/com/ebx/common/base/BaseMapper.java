
package com.ebx.common.base;

/**
 * 定义公共数据库调用方法
 * 
 * @author hubin
 * @date 2018-04-23
 */
public interface BaseMapper<T> extends com.baomidou.mybatisplus.mapper.BaseMapper<T> {

}
