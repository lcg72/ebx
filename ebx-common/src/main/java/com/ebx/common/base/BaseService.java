package com.ebx.common.base;

import com.baomidou.mybatisplus.service.IService;

/**
 * 定义公共业务调用方法
 * 
 * @author hubin
 * @date 2018-04-23
 */
public interface BaseService<T> extends IService<T> {

}
