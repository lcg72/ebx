package com.ebx.common.base;

import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

/** 
 * 通用业务类
 * @author ysong
 * @date 2018-04-23
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T>
		implements BaseService<T> {

	@Autowired
	protected M mapper;
}
