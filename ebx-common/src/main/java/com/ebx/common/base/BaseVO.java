
package com.ebx.common.base;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;

/**
 * 定义公共字段
 * 
 * @author hubin
 * @date 2018-04-23
 */
public class BaseVO<T extends Model<T>> extends Model<T> {
    
    private static final long serialVersionUID = 1L;
    
    /** 主键ID */
    private Long id;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    
}
