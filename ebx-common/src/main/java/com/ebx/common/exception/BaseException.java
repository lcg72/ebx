
package com.ebx.common.exception;

/**
 * 异常基类
 * 
 * @author hubin
 * @date 2018-04-23
 */
public class BaseException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;
    
    public BaseException() {
    }
    
    public BaseException(Throwable ex) {
        super(ex);
    }
    
    public BaseException(String message) {
        super(message);
    }
    
    public BaseException(String message, Throwable ex) {
        super(message, ex);
    }
    
}
