
package com.ebx.common.model;

import java.util.List;

/**
 * ajax请求返回VO
 * 
 * @author ysong
 * @date 2018年4月23日
 */
public class ResponseVO {
    
    /** 返回码 */
    private int respCode;
    
    /** 返回信息 */
    private String respMsg;
    
    /** 返回集合 */
    private Object respObj;
    
    /** 返回集合 */
    private List<Object> respList;
    
    public Object getRespObj() {
        return respObj;
    }
    
    public void setRespObj(Object respObj) {
        this.respObj = respObj;
    }
    
    public int getRespCode() {
        return respCode;
    }
    
    public void setRespCode(int respCode) {
        this.respCode = respCode;
    }
    
    public String getRespMsg() {
        return respMsg;
    }
    
    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }
    
    public List<Object> getRespList() {
        return respList;
    }
    
    public void setRespList(List<Object> respList) {
        this.respList = respList;
    }
    
}
