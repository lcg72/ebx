package com.ebx.common.testcase.mapper;

import com.ebx.common.base.BaseMapper;
import com.ebx.common.testcase.model.UserVO;

public interface UserMapper extends BaseMapper<UserVO> {

}
