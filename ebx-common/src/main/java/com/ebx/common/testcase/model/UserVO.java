package com.ebx.common.testcase.model;

import com.baomidou.mybatisplus.annotations.TableName;
import com.ebx.common.base.BaseVO;

@TableName("p_user")
public class UserVO extends BaseVO<UserVO> {

	private static final long serialVersionUID = 1L;

	private String name;

	private Integer userAge;

	public Integer getUserAge() {
		return userAge;
	}

	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
