package com.ebx.common.testcase.service;

import com.ebx.common.base.BaseService;
import com.ebx.common.testcase.model.UserVO;

public interface UserService extends BaseService<UserVO> {

}
