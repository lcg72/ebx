package com.ebx.common.testcase.service.impl;

import org.springframework.stereotype.Service;

import com.ebx.common.base.BaseServiceImpl;
import com.ebx.common.testcase.mapper.UserMapper;
import com.ebx.common.testcase.model.UserVO;
import com.ebx.common.testcase.service.UserService;

@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, UserVO> implements UserService {

}
