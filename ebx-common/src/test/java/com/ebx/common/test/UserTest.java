package com.ebx.common.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.mybatisplus.plugins.Page;
import com.ebx.common.testcase.model.UserVO;
import com.ebx.common.testcase.service.UserService;
import com.ebx.test.BaseJunit;

public class UserTest extends BaseJunit {

	@Autowired
	UserService userService;

	@Test
	public void insertUser() {
		UserVO userVO = new UserVO();
		userVO.setName("test");
		userVO.setUserAge(10);
		this.userService.insert(userVO);

		Page<UserVO> page = new Page<>();
		page.setCurrent(2);
		page.setSize(2);
		page = this.userService.selectPage(page);
		System.out.println(page.getPages());
		System.out.println(page.getRecords().get(0).getName());
	}
}
